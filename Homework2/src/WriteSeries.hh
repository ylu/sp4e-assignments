#include "DumperSeries.hh"

#ifndef WRITESERIES_HH
#define WRITESERIES_HH

// Declaration of class WriteSeries inheriting from DumperSeries to output series calculation process into a file
class WriteSeries: public DumperSeries {
    public:
        // Constructor
        WriteSeries(Series& series, int maxiter);
        // Destructor
        virtual ~WriteSeries();

        int maxiter;
        char seperator = ' ';
        unsigned int precision;
        std::string fileName = "result";

        void setSeparator(char sep);
        void dump(std::ostream & os) override;
        void setPrecision(unsigned int prec) override;
};

#endif