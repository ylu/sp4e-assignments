#include "Series.hh"
#include <iostream>
#ifndef DUMPERSERIES_HPP
#define DUMPERSERIES_HPP


// Declaration of the Base Class DumperSeries 
class DumperSeries{
    public:

        DumperSeries(Series& series): series(series) {};
        virtual ~DumperSeries() {};

        virtual void dump(std::ostream & os) = 0;
        virtual void setPrecision(unsigned int precision) = 0;

    protected:
        Series & series;
};
#endif