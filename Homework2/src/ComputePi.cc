#include "ComputePi.hh"
#include <cmath>

// Constructor
ComputePi::ComputePi(){};
// Destructor
ComputePi::~ComputePi(){};

// Compute the value at k-th term
double ComputePi::computeTerm(unsigned int k) {
    return 1.0 / (1.0 * k * k);
}

// Compute the arithmetic series
// N: Maximum iterations to compute the series
double ComputePi::compute(unsigned int N) {
    Series::compute(N);
    return sqrt(6. * this->currentValue);
}

// Get the analytic prediction for Pi
double ComputePi::getAnalyticPrediction() {
    return M_PI;
}