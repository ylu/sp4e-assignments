#ifndef SERIES_HPP
#define SERIES_HPP

// Declaration of the Base Class Series. 
class Series {
    public:
        // Constructor
        Series();
        // Destructor
        virtual ~Series();

        unsigned int currentTerm = 0;
        double currentValue = 0;
    public:
        virtual double compute(unsigned int N) = 0;
        virtual void addTerm();
        virtual double computeTerm(unsigned int k) = 0;
        virtual double getAnalyticPrediction();
};

#endif