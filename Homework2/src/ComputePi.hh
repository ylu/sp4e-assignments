#include "Series.hh"
#ifndef COMPUTEPI_HPP
#define COMPUTEPI_HPP

// Declaration of class ComputePi inheriting from Series for Pi series calculation
class ComputePi: public Series {
    public:
        // Constructor
        ComputePi();
        // Destructor
        virtual ~ComputePi();

        double computeTerm(unsigned int k) override;
        double compute(unsigned int N) override;
        double getAnalyticPrediction() override;
};
#endif