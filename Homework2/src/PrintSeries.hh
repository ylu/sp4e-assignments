#include "DumperSeries.hh"

#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH

// Declaration of class PrintSeries inheriting from DumperSeries to print series calculation process
class PrintSeries: public DumperSeries {
    public:
        // Constructor
        PrintSeries(Series& series, int maxiter, int freq);
        // Destructor
        virtual ~PrintSeries();

        int maxiter;
        int freq;
        unsigned int precision;

        void dump(std::ostream & os = std::cout) override;
        void setPrecision(unsigned int prec) override;
};

#endif