#include "Series.hh"
#ifndef COMPUTEARITHMETIC_HPP
#define COMPUTEARITHMETIC_HPP

// Declaration of class ComputeArithmetic inheriting from Series for arithmetic series calculation
class ComputeArithmetic: public Series {
    public:
        // Constructor
        ComputeArithmetic();
        // Destructor
        virtual ~ComputeArithmetic();

        double computeTerm(unsigned int k) override;
        double compute(unsigned int N) override;
        double getAnalyticPrediction() override;
};
#endif