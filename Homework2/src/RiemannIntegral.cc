#include "RiemannIntegral.hh"
#include <cmath>
#include <iostream>

RiemannIntegral::RiemannIntegral(double a, double b, std::string f, unsigned int N) : Series() {
    this->a = a;
    this->b = b;
    this->f = f;
    this->N = N;
    if (this->f != "cubic" && this->f != "cos" && this->f != "sin")
        throw std::invalid_argument("Invalid function for calculating Riemann Integral.");
};

RiemannIntegral::~RiemannIntegral(){};

double RiemannIntegral::computeTerm(unsigned int k) {
    double delta_x = (this->b - this->a) / this->N;
    if (f == "cubic")
        return pow(this->a + k * delta_x, 3.) * delta_x;
    else if (f == "cos")
        return cos(this->a + k * delta_x) * delta_x;
    else if (f == "sin")
        return sin(this->a + k * delta_x) * delta_x;
    else
        return 0;
}

double RiemannIntegral::compute(unsigned int N) {
    return Series::compute(this->N);
}