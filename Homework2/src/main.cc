#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "dumper_series.hh"
#include "RiemannIntegral.hh"
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
/* -------------------------------------------------------------------------- */


int main(int argc, char **argv) {
   
    // String stream for handling arguments
    std::stringstream ss;
    // Type of computation to be done with this program
    std::string computeType;
    // Initialize the pointer to Series
    Series* ptr = NULL;

    // Input arguments with sstream
    for (int i = 1; i < argc; i++){
        ss << argv[i] << " ";
    }
    computeType = argv[1];
    if (computeType == "Series"){
        // arguments for Series and Dumper
        unsigned int maxiter;   // maximum iteration to compute series
        std::string seriesType; // type of series to compute, options: [Arithmetic, Pi]
        std::string dumpType;   // type of dumping, options: [Print, Write]
        unsigned int freq;      // dumping frequency
        unsigned int precision; // decimal precision for the dumping results
        std::string ext;        // extension name for the document to write
        ss >> computeType >> maxiter >> seriesType >> dumpType >> freq >> precision >> ext;

        // Exercise 2
        if (seriesType == "Arithmetic") {
            ptr = new ComputeArithmetic();
        } else if (seriesType == "Pi") {
            ptr = new ComputePi();
        } else {
            std::cout << "Please specify the type of series" << std::endl;
        }
        if (seriesType == "Arithmetic" || seriesType == "Pi") {
            std::cout << "Exercise 2: Output result of " << seriesType << " series: " << ptr->compute(maxiter) << std::endl;
        }

        // Exercise 3
        std::cout << "Exercise 3,4,5: Dump series calculation through " << dumpType << std::endl;
        DumperSeries* dumpPtr = NULL;
        if (dumpType == "Print") {
            // If dumpType is Print, set printing frequency and cout precision, then print on the screen
            dumpPtr = new PrintSeries(*ptr, maxiter, freq);
            PrintSeries* printPtr = dynamic_cast<PrintSeries*> (dumpPtr);
            printPtr -> setPrecision(precision);
            printPtr -> dump();
            // std::cout << *dumpPtr;

        } else if (dumpType == "Write") {
            // If dumpType is Write, set separator and precision, and output a file with given extension
            char separator;
            if (ext == "csv") {
                separator = ',';
            } else if (ext == "txt") {
                separator = ' ';
            } else if (ext == "psv") {
                separator = '|'; 
            }
            dumpPtr = new WriteSeries(*ptr, maxiter);
            WriteSeries* writePtr = dynamic_cast<WriteSeries*> (dumpPtr);
            writePtr -> setSeparator(separator);
            writePtr -> setPrecision(precision);
            std::ofstream os(writePtr->fileName);
            writePtr->dump(os);


            // Exercise 4 - output result again with new operator <<, txt by default
            std::ofstream file;
            file.open("result.txt");
            file << *writePtr;
        }
        
    } else if (computeType == "Integral") {
        // Exercise 6
        // If function f to be integrated is specified, run Riemann Integral and print result on the screen

        // arguments for Riemann integral
        double a;   // lower bound for the integral
        double b;   // upper bound for the integral
        std::string f;  // function to be integrated
        unsigned int N; // number of intervals to compute integral
        ss >> computeType >> a >> b >> f >> N;

        if (f == "cubic" || f == "sin" || f == "cos" ) {
            ptr = new RiemannIntegral(a, b, f, N);
            std::cout << "Exercise 6: Output result of Riemann Integral is " << ptr->compute(N) << std::endl;
        } else {
            std::cout << "Exercise 6: Please select a valid function to compute Integral" << std::endl;
        }
    }
    
    return 0;
}