#include "WriteSeries.hh"
#include <string>
#include <fstream>
#include <iostream>

// Constructor
WriteSeries::WriteSeries(Series& series, int maxiter) : DumperSeries(series) {
    this->maxiter = maxiter; // Initialize the maximum iteration of series calculation to write 
}
// Destructor
WriteSeries::~WriteSeries(){};

// Function to set the separator and extension name for the output file
void WriteSeries::setSeparator(char sep) {
    this->seperator = sep;
    std::string ext;
    if (this->seperator == ',') {
        ext = ".csv";
    } else if (this->seperator == ' ') {
        ext = ".txt";
    } else if (this->seperator == '|') {
        ext = ".psv";
    }
    this->fileName += ext;
}

// Function to set precision for the output result
void WriteSeries::setPrecision(unsigned int prec) {
    this->precision = prec;
}

void WriteSeries::dump(std::ostream & os) {

    double res;
    for (int i = 1; i <= this->maxiter; i++) {
        // As required, frequency = 1
        res = this->series.compute(i);
        os << res << std::endl;
    }

}