#include "PrintSeries.hh"
#include <cmath>
#include <iostream>
#include <iomanip>

// Constructor
PrintSeries::PrintSeries(Series& series, int maxiter, int freq) : DumperSeries(series) {
    this->maxiter = maxiter; // Initialize maximum iteration of series calculation to print
    this->freq = freq; // Initialize printing freuqency
};
// Destructor
PrintSeries::~PrintSeries(){};

// Function to set precision for the printing
void PrintSeries::setPrecision(unsigned int prec) {
    this->precision = prec;
}

// Function to print Current Iteration, Current Series Value, and Difference between our value and analytic prediction
// Input: os is the output stream
void PrintSeries::dump(std::ostream & os) {

    double res;
    double analyticPred;

    for (int i = 1; i <= this->maxiter/this->freq; i++) {
        res = this->series.compute(i * this->freq);
        analyticPred = this->series.getAnalyticPrediction();
        if (!std::isnan(analyticPred)) {
            os << "Iteration: " << i * this->freq << " ";
            os << "Current value: " << res << " ";
            os << "Difference: ";
            os << std::setprecision(this->precision) <<  abs(analyticPred - res) << std::endl;
        }
    }
}

