#include "Series.hh"
#include <cmath>

// Constructor
Series::Series(){};
// Destructor
Series::~Series(){};

// Function to compute the current sum value at next term
void Series::addTerm() {
    this->currentTerm += 1;
    this->currentValue += this->computeTerm(this->currentTerm);
}

// Function to compute the series
// N is the maximum iteration to compute the series
double Series::compute(unsigned int N) {
    int sum = 0;

    if (this->currentTerm <= N) {
        N -= this->currentTerm;
    } else {
        this->currentValue = 0;
        this->currentTerm = 0;
    }

    for (int i = 0; i < N; i++) {
        this->addTerm();
    }
    return this->currentValue;
}

// Function to output analytic prediction of a series. Return NAN by default.
double Series::getAnalyticPrediction() {
    return nan("");
}