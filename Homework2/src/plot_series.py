import matplotlib.pyplot as plt
import argparse

def plot_series(file, n):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    numerical = []
    
    # Read from generated file
    with open(file, 'r') as file:
        numerical = []
        x = []
        for i, line in enumerate(file):
            if i % n == 0:
                numerical.append(float(line.strip()))
                x.append(i)

    # Plot
    ax.plot(x, numerical, marker='.', label='Numerical')
    ax.set_xlabel(r'$k$')
    ax.set_ylabel(r'Series')
    ax.legend()
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Read and plot.")
    parser.add_argument("file", help="The path to the file.")
    parser.add_argument("n", type=int, help="Dumper frequency.")

    args = parser.parse_args()
    plot_series(args.file, args.n)
