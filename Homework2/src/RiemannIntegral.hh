#include "Series.hh"
#include <string>
#ifndef RIEMANN_H
#define RIEMANN_H

class RiemannIntegral : public Series {
    public:
        // Constructor
        RiemannIntegral(double a, double b, std::string f, unsigned int N);
        // Destructor
        virtual ~RiemannIntegral();

        double a, b;
        std::string f;
        unsigned int N;

        double computeTerm(unsigned int k) override;
        double compute(unsigned int N) override;
};

#endif