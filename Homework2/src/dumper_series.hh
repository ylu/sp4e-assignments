#include "DumperSeries.hh"
#ifndef DUMPER_SERIES_HH
#define DUMPER_SERIES_HH

// Declaration of the new operator <<
inline std::ostream & operator << (std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
}

#endif

