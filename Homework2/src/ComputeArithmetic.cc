#include "ComputeArithmetic.hh"

// Constructor
ComputeArithmetic::ComputeArithmetic(){};
// Destructor
ComputeArithmetic::~ComputeArithmetic(){};


// Compute the value at k-th term
double ComputeArithmetic::computeTerm(unsigned int k) {
    return 1.0 * k;
}

// Compute the arithmetic series
// N: Maximum iterations to compute the series
double ComputeArithmetic::compute(unsigned int N){
    return Series::compute(N);
}

// Get the analytic prediction for arithmetic series
double ComputeArithmetic::getAnalyticPrediction() {
    return this->currentTerm * (this->currentTerm + 1.0) / 2.0;
}