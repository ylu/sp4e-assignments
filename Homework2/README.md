# Homework 2 - C++ Classes

## Description 
This is code repository for Homework 2 - C++ classes, where we program a family of objects with C++ to compute Series or Riemann Integral and to dump them.

## Requirements
* cmake_minimum_required(VERSION 2.6)
* python == 3.7
* matplotlib == 3.5.2

## Instructions to compile the code
```
cd Homework2
mkdir build && cd build
cmake ..
make
```

## Instructions to run the code
### Brief description to the code
To better factorize the function of this program and to make the input more clear, we decide to separate the Series computation and Riemann Integral computation function in main.cc. To run each calculation, please specify the type of computation in the first argument and then input other necessary arguments in order. More details to run the code are presented below.


### Required arguments to run the code
**Mandatory arguments** \
`computeType` : determine the program to compute series or integral, options [Series, Integral]. \
**Required arguments to compute series** \
`maxiter` : maximum iteration to compute series, e.g. 10.\
`seriesType` : type of series to compute, options: [Arithmetic, Pi].\
`dumpType` : type of dumping, options: [Print, Write].\
`freq` : dumping frequency, e.g. 1.\
`precision` : decimal precision for the dumping results, e.g. 5.\
`ext` : extension name for the document to write, options: [csv, txt, psv].\
**Required arguments to compute integral** \
`a` : lower bound for the integral, e.g. 0.\
`b` : upper bound for the integral, e.g. 1.\
`f` : function to be integrated, options: [cubic, sin, cos].\
`N` : number of intervals to compute integral, e.g. 100.\
*Note: The above arguments should be input to the program in order*

### Examples
The following command computes Pi series with maximum 10 iterations, and prints results on the screen with printing frequency of 1 and decimal precision of 5
```
src/main Series 10 Pi Print 1 5 
```
The following command computes Arithmetic series with maximum 20 iterations, and outputs results to a file with csv extension
```
src/main Series 20 Arithmetic Write 1 5 csv
```
The following command computes Riemann Integral for sin() function and prints results on the screen. The lower bound and upper bound for Integral are 0 and 3.14, and the iteration for the integral is 200
```
src/main Integral 0 3.14 sin 200
```
To plot the series with python, input the file name and desired frequency. The following command plots the series for every 2 iterations
```
python ../src/plot_series.py result.csv 2
```


## Answers to the questions
Exercise 2.1 \
The best strategy to divide work is writing a base class together and then each one write a child class. For example, once the class Series is done, we can write class ComputeArithmetic and ComputePi separately and same for the dumping. 

Exercise 5.1 \
When the speicified maximum iteration is N, the complexity to calculate the series is O(N) and the global complexity of the program when calculate series at each frequency is O(N^2). 

Exercise 5.4 \
If we save the status of currentValue and currentTerm, we don't need to repeatly compute the series and the global complexity is O(N).

Exercise 5.5 \
The best achievable complexity remains O(N) because the number of performed calculations is the same. 

Exercise 6.4 \
For $x^3$, N = 55. \
For $cos(x)$, N = 320. \
For $sin(x)$, N = 80.