#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
ComputeTemperature::ComputeTemperature() {
    this->rho = 1.0;
    this->capacity = 1.0;
    this->conductivity = 1.0;
    this->deltat = 0.0001;
}

void ComputeTemperature::compute(System& system) {

    int nbParticles = system.getNbParticles();
    int N = sqrt(nbParticles);
    Matrix<complex> theta_n(N);    
    Matrix<complex> heat_v(N);

    // Get Temperature and HeatRate for each particle
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            Particle& particle = system.getParticle(i * N + j);
            MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(particle);
            theta_n(i, j) = materialPt.getTemperature();
            heat_v(i, j) = materialPt.getHeatRate();
        }
    }

    // Calculate Fourier transform for theta_n (temperature) and heat_v (heat)
    Matrix<complex> theta_n_hat = FFT::transform(theta_n);
    Matrix<complex> heat_v_hat = FFT::transform(heat_v);

    // Calculate dthetahat_dt in frequency domain
    Matrix<complex> dthetahat_dt(N);
    Matrix<std::complex<int>> freq = FFT::computeFrequencies(N);;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            Real qx = (2 * M_PI / N) * freq(i, j).real();
            Real qy = (2 * M_PI / N) * freq(i, j).imag();
            dthetahat_dt(i, j) = (heat_v_hat(i, j) - this->conductivity * theta_n_hat(i, j) * (qx * qx + qy * qy)) / (this->rho * this->capacity);
        }
    }
    
    // Calculate inverse Fourier transform for dthetahat_dt
    Matrix<complex> dtheta_dt = FFT::itransform(dthetahat_dt);

    // Calculate Euler integration + For Q4.5, boundary particles don't evolve with time
    for (int i = 1; i < N - 1; i++) {
        for (int j = 1; j < N - 1; j++) {
            Particle& particle = system.getParticle(i * N + j);
            MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(particle);
            materialPt.getTemperature() = theta_n(i, j).real() + this->deltat * dtheta_dt(i, j).real();

        }
    }
}

/* -------------------------------------------------------------------------- */
