import numpy as np
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--radius', help="Radius", type=float, default=1)
    parser.add_argument('--number', help="number of particles", type=int, default=512)
    parser.add_argument('--span', help="domain span", type=int, default=2)
    parser.add_argument('--filename', help="name of generated input file", default="input.csv")

    args = parser.parse_args()

    radius = args.radius
    nbParticles = args.number
    gridspan = args.span
    filename = args.filename

    # Initialize particle grid
    x = np.linspace(-gridspan/2, gridspan/2, nbParticles)
    y = np.linspace(-gridspan/2, gridspan/2, nbParticles)
    z = np.zeros((nbParticles, nbParticles))
    zero_init = np.zeros((nbParticles, nbParticles)).flatten()

    xx, yy = np.meshgrid(x, y)
    heat_v = np.zeros((nbParticles, nbParticles))
    
    ## Ex4.2
    # initial_temperatures = np.ones((nbParticles, nbParticles)).flatten() * 10.0
    
    ## Ex4.5
    initial_temperatures = np.zeros((nbParticles, nbParticles)).flatten() 
    for i in range(nbParticles):
        for j in range(nbParticles):
            if ((xx[i, j] ** 2 + yy[i, j] ** 2) < radius):
                heat_v[i, j] = 1
            else:
                heat_v[i, j] = 0

    # Head: x, y, z, vx, vy, vz, fx, fy, fz, mass, temperature, heat
    # Parameters [z, vx, vy, vz, fx, fy, fz, mass] are set zero in Ex4.5 & Ex4.6
    save_data = np.column_stack((xx.flatten(), yy.flatten(), z.flatten(), 
                                 zero_init, zero_init, zero_init, 
                                 zero_init, zero_init, zero_init,
                                 zero_init, initial_temperatures, heat_v.flatten()))
    np.savetxt(filename, save_data, delimiter=" ")
