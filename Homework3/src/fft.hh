#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {

    UInt N = m_in.size();
    Matrix<complex> m_out(N);
    fftw_plan p;
    
    auto in = reinterpret_cast<fftw_complex*>(m_in.data());
    auto out = reinterpret_cast<fftw_complex*>(m_out.data());

    p = fftw_plan_dft_2d(N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    fftw_execute(p); /* repeat as needed */
    
    fftw_destroy_plan(p);

    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    
    UInt N = m_in.size();
    Matrix<complex> m_out(N);
    fftw_plan p;
    
    auto in = reinterpret_cast<fftw_complex*>(m_in.data());
    auto out = reinterpret_cast<fftw_complex*>(m_out.data());
    p = fftw_plan_dft_2d(N, N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    
    fftw_execute(p); /* repeat as needed */
    m_out /= (N * N); // taking care of the coefficients of Fourier transform (2D -> N^2)
    
    fftw_destroy_plan(p);

    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
    Matrix<std::complex<int>> m_out(size);
    int N = size;

    std::vector<int> frequencies;

    if (size % 2 == 0) {
      for (int i = 0; i <= N / 2 - 1; ++i) 
        frequencies.push_back(i);
      for (int i = -N / 2; i <= -1; ++i) 
        frequencies.push_back(i);
    }
    else {
      for (int i = 0; i < (N - 1) / 2; ++i) 
        frequencies.push_back(i);
      for (int i = -(N - 1) / 2; i < -1; ++i) 
        frequencies.push_back(i);
    }

    for (auto&& entry : index(m_out)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      auto& val = std::get<2>(entry);

      val.real(frequencies[i]);
      val.imag(frequencies[j]);
    }

    return m_out;
}

#endif  // FFT_HH
