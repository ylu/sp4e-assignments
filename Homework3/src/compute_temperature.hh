#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

public:
  // Constructor
  ComputeTemperature();
  virtual ~ComputeTemperature() {};
  // Virtual implementation
  //! Penalty contact implementation
  void compute(System& system) override;
  Real rho;
  Real capacity;
  Real conductivity;
  Real deltat;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
