#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>

/*****************************************************************/
// Fixture class
class RandomParticles : public ::testing::Test {
protected:
  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> particles;
    UInt nbParticles = 512;

    for (UInt i = 0; i < nbParticles; ++i) {
      for (UInt j = 0; j < nbParticles; ++j) {
        MaterialPoint pt;
        // initialize location for a domain [-1, 1] x [-1, 1]
        pt.getPosition()[0] = - gridSpan / 2 + i * gridSpan / (nbParticles - 1);
        pt.getPosition()[1] = - gridSpan / 2 + j * gridSpan / (nbParticles - 1);
        pt.getPosition()[2] = 0.0;
        particles.push_back(pt);
      }
    }

    for (auto& p : particles) {
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }
  }

  System system;
  // Initialize parameters
  Real rho = 1.0;
  Real capacity = 1.0;
  Real conductivity = 1.0;
  Real deltat = 0.0001;
  UInt timestep = 10;
  Real gridSpan = 2;
};

TEST_F(RandomParticles, homogeneous) {
  
  std::cout << "Ex4.2 - Validating test for an initial homogeneous temperature and no heat flux" << std::endl;
  // Set homogeneous initial temperature
  Real const_temperature = 100.0;
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    materialPt.getTemperature() = const_temperature;
    materialPt.getHeatRate() = 0.0;
  }

  // Initialize parameters
  auto computeTemperature = std::make_shared<ComputeTemperature>();
  computeTemperature->rho = rho;
  computeTemperature->capacity = capacity;
  computeTemperature->conductivity = conductivity;
  computeTemperature->deltat = deltat;
  
  // Calculate temperature for every particle
  for (UInt i = 0; i < timestep; ++i) {
    computeTemperature->compute(system);
  }

  // Launch test
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    ASSERT_NEAR(materialPt.getTemperature(), const_temperature, 1e-6);
  }
};

TEST_F(RandomParticles, sinVolumetricHeatSource) {
  
  std::cout << "Ex4.3 - Validating test for a sin volumetric heat source" << std::endl;
  // Initialize temperature and heatrate from sin volumetric heat source
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    double x = p.getPosition()[0];
    materialPt.getTemperature() = sin(2 * M_PI * x / gridSpan);
    materialPt.getHeatRate() = (2 * M_PI / gridSpan) * (2 * M_PI / gridSpan) * 
                              sin(2 * M_PI * x / gridSpan);
  }

  // Initialize parameters
  auto computeTemperature = std::make_shared<ComputeTemperature>();
  computeTemperature->rho = rho;
  computeTemperature->capacity = capacity;
  computeTemperature->conductivity = conductivity;
  computeTemperature->deltat = deltat;
  
  // Calculate temperature for every particle
  for (UInt i = 0; i < timestep; ++i) {
    computeTemperature->compute(system);
  }

  // Launch test
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    double x = p.getPosition()[0];
    double y = p.getPosition()[1];
    ASSERT_NEAR(materialPt.getTemperature(), sin(2 * M_PI * x / gridSpan), 0.01);
  }
};



TEST_F(RandomParticles, stepVolumetricHeatSource) {
  
  std::cout << "Ex4.4 - Validating test for a step volumetric heat source" << std::endl;
  // Initialize temperature and heatrate from step volumetric heat source
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    double x = p.getPosition()[0];
    if (x <= -0.5) {
      materialPt.getTemperature() = -x - 1;  
    } else if (x > 0.5) {
      materialPt.getTemperature() = -x + 1;
    } else {
      materialPt.getTemperature() = x;
    }
    if (x == 0.5) {
      materialPt.getHeatRate() = 1;
    } else if (x == -0.5) {
      materialPt.getHeatRate() = -1;
    } else {
      materialPt.getHeatRate() = 0;
    }

  }

  // Initialize parameters
  auto computeTemperature = std::make_shared<ComputeTemperature>();
  computeTemperature->rho = rho;
  computeTemperature->capacity = capacity;
  computeTemperature->conductivity = conductivity;
  computeTemperature->deltat = deltat;
  
  // Calculate temperature for every particle
  for (UInt i = 0; i < timestep; ++i) {
    computeTemperature->compute(system);
  }

  // Launch test
  for (auto& p: system) {
    MaterialPoint& materialPt = dynamic_cast<MaterialPoint&>(p);
    double x = p.getPosition()[0];
    double y = p.getPosition()[1];
    double test_temp = 0.0;
    if (x <= -0.5) {
        test_temp = -x - 1;  
    } else if (x > 0.5) {
      test_temp = -x + 1;
    } else {
      test_temp = x;
    }
    ASSERT_NEAR(materialPt.getTemperature(), test_temp, 0.01);
  }
};
