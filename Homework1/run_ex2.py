import os
import numpy as np
import argparse
from GMRES import gmres
from optimizer import plot_result, scipy_optimize

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    
    parser.add_argument('--matrixA', '-A', type=float, help='Input matrix A in one row', nargs='+')
    parser.add_argument('--nrow', type=int, help='Number of rows of A')
    parser.add_argument('--vectorb', '-b', type=float, help='Input vector b in one row', nargs='+')
    parser.add_argument('--x0', type=float, help='Input x0 in one row', nargs='+')
    parser.add_argument('--minimizer', type=str, default='GMRES', help='Choose minimizer from GMRES, BFGS, lgmres')
    parser.add_argument('--plot', action='store_true', help='Plot the trace of optimization')
    parser.add_argument('--save_path', type=str, default='./results', help='Save results in PDF')
    parser.add_argument('--miter', type=int, default=100, help='Input max iteration for GMRES')
    parser.add_argument('--thresh', type=float, default=1e-6, help='Input threshold for GMRES')
    
    args = parser.parse_args()

    print("Run exercise 2")

    A = np.array(args.matrixA)
    A = A.reshape(args.nrow, A.shape[0]//args.nrow)
    b = np.array(args.vectorb)
    x0 = np.array(args.x0)
    assert A.shape[0] == A.shape[1]
    assert A.shape[0] == b.shape[0]
    assert A.shape[1] == x0.shape[0]
    minimizer = args.minimizer
    save_path = args.save_path
    miter = args.miter
    thresh = args.thresh

    # Select an optimizer and run optimization
    if minimizer == 'GMRES':
        x, e, xk_iter = gmres(A, b, x0, miter=miter, threshold=thresh)
    elif minimizer == 'BFGS':
        x, xk_iter = scipy_optimize(A, b, x0, 'BFGS')
        x = x.x
    elif minimizer == 'lgmres':
        x, xk_iter = scipy_optimize(A, b, x0, 'lgmres')
        x = x[0]
    else:
        raise Exception("Minimizer does not exist.")
    
    print(f"Solution: {x}")
    
    # Plot and save results 
    if args.plot:
        if save_path:
            os.makedirs(save_path, exist_ok=True)
            save_path = os.path.join(save_path, minimizer+'.pdf')
        plot_result(A, b, xk_iter, save_path=save_path)
    

