# Homework 1 - Generalized minimal residual method

## Description 
This is the homework 1 where we use different methods to solve a linear system Ax = b and plot the trace of optimization.

## Requirements
* python == 3.7
* numpy == 1.21.2
* scipy == 1.8.1
* matplotlib == 3.5.2


## Instructions to run the code
### Exercise 1
To run exercise 1, directly execute `optimizer.py`.
```
python optimizer.py
```

### Exercise 2
To run exercise 2, execute `run_ex2.py` and specify necessary parameters as follows.
```
python run_ex2.py --matrixA 8 1 1 3 --nrow 2 --vectorb 2 4 --x0 0 0 --minimizer BFGS --plot --save_path ./results
```

### Parameters in Exercise 2
`--matrixA` or `-A` : Input matrix A in one row.\
`--nrow` : Number of rows in A matrix.\
`--vectorb` or `-b` : Input b vector.\
`--x0` : Input initial guess for solution x.\
`--minimizer` : The minimizer to be used. Options: "BFGS", "lgmres" or "GMRES".\
`--miter` : Input maximum iteration for the custom GMRES, by default 100.\
`--thresh` : Input threshold for the custom GMRES, by default 1e-6.\
`--plot` : (Optional) Generate an interactive plot with the trace of optimization.\
`--save_path` : (Optional) Specify a path to save plot results.

