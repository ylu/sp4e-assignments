import numpy as np
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres
import matplotlib.pyplot as plt

def plot_result(A, b, xk_iter, save_path=None):
    """ Plot the trace of optimization and also surface of the function

    Args:
        A (numpy array): Coefficient of the left hand side.
        b (numpy array): Right hand side of the equation.
        xk_iter (list): iterations of x at each optimization step.

    Returns:
        none
    """
    def S(x, A, b):
        return 1 / 2 * (x.T @ A @ x) - x.T @ b
    
    # Setting-up grid for ploting the surface
    x = np.linspace(-3, 3, 50)
    y = np.linspace(-3, 3, 50)
    xv, yv = np.meshgrid(x, y)

    # Compute z for each point on the grid
    z = np.zeros(xv.shape)
    for i in range(x.shape[0]):
        for j in range(x.shape[0]):
            z[i, j] = S(np.array([xv[i, j], yv[i, j]]), A, b)

    # Coordinates for the trace of optimization
    z_iter = [S(x, A, b) for x in xk_iter] 
    x_iter = [x[0] for x in xk_iter]
    y_iter = [x[1] for x in xk_iter]

    # Plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=45, azim=135)
    ax.plot_surface(xv, yv, z, cmap="autumn_r", lw = 0.5, rstride=1, cstride=1, alpha=0.5)
    ax.contour3D(xv, yv, z, cmap="binary")
    ax.contour(x,y,z)
    
    ax.plot(x_iter, y_iter, z_iter, linestyle='--', marker='o', color='b')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    plt.show()

    # Save figure 
    if save_path:
        fig.savefig(save_path)


def scipy_optimize(A, b, x0, minimizer):
    """Use scipy to solve a linear system Ax = b.

    Args:
        A (numpy array): Coefficient of the left hand side.
        b (numpy array): Right hand side of the equation.
        x0 (numpy array): initial guess of the solution
        minimizer (string): has to be "BFGS" or "lgmres".

    Returns:
        xk_iter: iterations of x at each optimization step.
        res: optimized results.
    """

    # Quadratic function 
    def S(x, A, b):
        return 1 / 2 * (x.T @ A @ x) - x.T @ b
    
    # wrap the function with input from command line
    S_ = lambda x: S(x, A, b)

    # Track the progress of optimization using callback
    # xk_iter stores x values at each step
    xk_iter = [x0]
    def callback(xk):
        # print(f"Current solution: {xk}")
        xk_iter.append(xk)

    # choose optimizer based on chosen minimizer 
    def optimizer(S_, x0, minimizer):
        if minimizer == 'BFGS':
            return minimize(S_, x0, method=minimizer, callback=callback)
        elif minimizer == 'lgmres':
            return lgmres(A, b, x0, callback=callback)
        else:
            raise Exception("Minimizer does not exist.")
    
    # start optimization
    res = optimizer(S_, x0, minimizer)

    return res, xk_iter


if __name__ == '__main__':
    print("Run exercise 1")

    # Equation to solve
    A = np.array([[8, 1], [1, 3]])
    b = np.array([2, 4])
    x0 = np.zeros(2)
    
    # BFGS optimizer
    x, xk_iter = scipy_optimize(A, b, x0, 'BFGS')
    plot_result(A, b, xk_iter)

    # lgmres optimizer
    # x, xk_iter = scipy_optimize(A, b, x0, 'lgmres')
    # plot_result(A, b, xk_iter)


