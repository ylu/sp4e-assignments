import numpy as np

def dot_product(a, b):
    """ Dot product of two vectors using numpy.einsum()"""
    return np.einsum('i,i', a, b)

def matrix_mul(a, b):
    """ Apply Multiplication between two matrixs or 
        one matrix and one vector using numpy.einsum()
    """
    if a.ndim == 2 and b.ndim == 1:
        return np.einsum('ij, j', a ,b)
    elif a.ndim == 1 and b.ndim == 2:
        return np.einsum('j, ij', a, b)
    elif a.ndim == 2 and b.ndim == 2:
        return np.einsum('ij, jk -> ik', a, b)

def normalization(a):
    """ Normalize a vector using numpy.einsum()"""
    return np.sqrt(np.einsum('i, i', a, a))


def arnoldi(A, Q, k): 
    """ Arnoldi function """   
    h = np.zeros(k + 2)
    q = matrix_mul(A, Q[:, k])
    for i in range(k + 1):
        h[i] = dot_product(q.T, Q[:, i])
        q = q - h[i] * Q[:, i]
    h[k + 1] = normalization(q)
    q = q / h[k + 1]
    return h, q
    
def givens_rotation(v1, v2):
    """ Calculate the givens rotation matrix """
    if v1 == 0 and v2 == 0:
        cs = 0
        sn = 0
    else:
        t = np.sqrt(v1 ** 2 + v2 ** 2)
        cs = v1 / t
        sn = v2 / t
    return cs, sn 


def apply_givens_rotation(h, cs, sn, k):
    """ Apply given rotations to H col"""
    
    # Apply for i-th column
    for i in range(k):
        temp = cs[i] * h[i] + sn[i] * h[i + 1]
        h[i + 1] = -sn[i] * h[i] + cs[i] * h[i + 1]
        h[i] = temp
    
    # update the next sin cos values for rotation
    cs_k, sn_k = givens_rotation(h[k], h[k + 1])
    
    # eliminate H[k+1, k]
    h[k] = cs_k * h[k] + sn_k * h[k + 1]
    h[k + 1] = 0.0
    
    return h, cs_k, sn_k
        

def gmres(A, b, x, miter=100, threshold=1e-6):
    """ Implementation of Generalized Minimal Residual Method (RMRES)
    
    Args:
        A:          array, matrix A
        b:          array, vector b
        x:          array, initial guess x0
        miter:      int, maximum iteration, by default 100
        threshold:  float, error threshold to break iteration, by default 1e-6
    
    Returns:
        x:          array, solution 
        e:          list, list of errors in each iteration
        xk_iter     list, list of temporary solutions after each iteration
    """

    r = b - matrix_mul(A ,x)
    b_norm = normalization(b)
    r_norm = normalization(r)
    err = r_norm / b_norm
    e = [err]    
    
    # initialization
    sn = np.zeros(miter)
    cs = np.zeros(miter)
    el = np.zeros(miter + 1)
    el[0] = 1
    H = np.zeros((miter + 1, miter))
    Q = np.zeros((r.shape[0], miter))
    Q[:, 0] = r / r_norm
    beta = r_norm * el

    # save xk_iter for plot
    xk_iter = [x]

    for k in range(miter):

        # Run arnoldi
        H[:k + 2, k], Q[:, k + 1] = arnoldi(A, Q, k)
        
        # eliminate the last element in H k-th row and update the rotation matrix
        H[:k + 2, k], cs[k], sn[k] = apply_givens_rotation(H[:k + 2, k], cs, sn, k)
        
        # update the residual vector
        beta[k + 1] = -sn[k] * beta[k]
        beta[k] = cs[k] * beta[k]
        err = np.abs(beta[k + 1] / b_norm)
        
        # save the error
        e.append(err)

        # break if err reaches threshold
        if err < threshold: 
            break

        # save xk_iter for plot
        y = matrix_mul(np.linalg.inv(H[:k+1, :k+1]), beta[:k+1])
        x_tmp = x + matrix_mul(Q[:, :k + 1], y)
        xk_iter.append(x_tmp)
    
    # calculate the result
    y = matrix_mul(np.linalg.inv(H[:k+1, :k+1]), beta[:k+1])
    x = x + matrix_mul(Q[:, :k + 1], y)
    
    # save xk_iter for plot
    xk_iter.append(x)
    
    return x, e, xk_iter

if __name__ == '__main__':
    """ Try different (A,b,x0) here to test our implemented GMRES solver """

    A = np.array([[8,1],[1,3]])
    b = np.array([2,4])
    x0 = np.array([1,10])

    # A = np.array([[8, 1, 4], [1, 3, 9], [2, 13, 11]])
    # b = np.array([2, 4, 7])
    # x0 = np.array([1, 2, 3])

    x, e, xk_iter = gmres(A, b, x0, 100, 1e-6)
    print("solution x: ", x, "\n")
    print("x_iteration: ", xk_iter, "\n")
    print("error: ", e, "\n")
