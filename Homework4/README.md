# Homework 4 - Pybind and Trajectory Optimization

## Description
This is code repository for Homework 4 - Pybind and Trajectory Optimization, where we use the external library Pybind11 to create Python bindings of the C++ Particles' code. 


## Requirements
* cmake_minimum_required(VERSION 2.6)
* python == 3.7
* FFTW
* GoogleTest
* Pybind


## Instructions to compile the code

```
cd Homework4
mkdir build && cd build
git submodule update --init
cmake .. -DUSE_PYTHON=ON
make
```


## Instructions to run the code

### Exercise 1, 2, 3, 4:
The python bindings for all necessary classess and their respective functions are written in `Homework4/src/pypart.cc`.

To run the simulation in exercise 4, execute the following command and the simulated trajectory will be saved in `Homework4/build/src/dumps` directory.
```
cd Homework4/build/src && mkdir dumps
python main.py 365 1 ../../src/init.csv planet 1
```
The parameters specified in the command are number of evolving steps, dumping frequency, input file, particle type, and timestep.

### Exercise 5, 6, 7:
The python functions required in exercise 5 and 6 are written in `Homework4/src/utils.py`.

To find the correct inital velocity for Mercury, run `optimization.py` with the following command:
```
cd Homework4/src
python optimization.py
```

After the optimization, it will output the following information in the terimnal:
```
Optimization terminated successfully.
         Current function value: 0.269948
         Iterations: 14
         Function evaluations: 28
Optimal scale factor:  [0.3989502]
Correct initial velocity:  [ 0.01859352 -0.00678052 -0.00226043]
```
Meanwhile, a new inital status with corrected velocity is dumped in `Homework4/src/init_scaled.csv`. The evolution of the error versus the scaling factor during the optimization is depicted below and also saved in `Homework4/result/optimization_result.png`
![Error vs scale factor](./result/optimization_result.png)

## Answer to the questions
Exercise 1 - Q2: \
Overloading the createSimulation function to take functor as argument allows for customizing the simulation in the python code by changing different compute classes.

Exercise 2 - Q2: \
We use the smart pointer std::shared_ptr to manage different references to Compute objects. 
