import os
from utils import *
from scipy import optimize
import matplotlib.pyplot as plt

param_values_at_iteration = []
function_values_at_iteration = []

def objective_function(scale):
    
    fixed_params = ('mercury', 'init.csv', 'trajectories', 'init_scaled.csv', 1, 365, 1)

    # Call the function with the fixed parameters and the varying parameter
    result = runAndComputeError(scale, *fixed_params)

    param_values_at_iteration.append(scale)
    function_values_at_iteration.append(result)

    return result

def callback_function(scale):
    objective_function(scale)

def main():
    
    # Initial guess for the variable parameter
    initial_guess = 0.5

    # Use fmin to find the optimal value for the variable parameter
    optimized_value = optimize.fmin(objective_function, initial_guess, callback=callback_function)

    # Plot the function values with respect to scale factor at each iteration
    plt.plot(param_values_at_iteration, function_values_at_iteration, marker='o', linestyle='-', color='b')

    plt.title('Error vs Scale factor at Each Iteration')
    plt.xlabel('Scale factor')
    plt.ylabel('Error')
    plt.show()
    os.makedirs('../result', exist_ok=True)
    plt.savefig('../result/optimization_result.png')
    
    print("Optimal scale factor: ", optimized_value)
    print("Correct initial velocity: ", np.array([0.0466061119752, -0.0169958959322, -0.00566593768503]) * optimized_value)

if __name__ == "__main__":
    main()