#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_temperature.hh"
#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "compute_interaction.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  // bind the routines here

  // python bindings for factory interface classes
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
        .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
        .def("createSimulation", 
          py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
          py::return_value_policy::reference,
          py::arg("fname"), py::arg("timestep"), py::arg("func"))
        .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
        .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);
  
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
        .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);
  
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
        .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);


  // python bindings for compute classes
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");
  
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");
        
  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
        .def(py::init<>())
        .def("setG", &ComputeGravity::setG, py::arg("G"));

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(py::init<>())
        .def_property("conductivity", &ComputeTemperature::getConductivity, [](ComputeTemperature& temp, Real val){temp.getConductivity() = val;})
        .def_property("L", &ComputeTemperature::getL, [](ComputeTemperature& temp, Real val){temp.getL() = val;})
        .def_property("capacity", &ComputeTemperature::getCapacity, [](ComputeTemperature& temp, Real val){temp.getCapacity() = val;})
        .def_property("density", &ComputeTemperature::getDensity, [](ComputeTemperature& temp, Real val){temp.getDensity() = val;})
        .def_property("deltat", &ComputeTemperature::getDeltat, [](ComputeTemperature& temp, Real val){temp.getDeltat() = val;});

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
        .def(py::init<Real>())
        .def("addInteraction", &ComputeVerletIntegration::addInteraction);
        
        
  // python binding for csvwriter class
  py::class_<CsvWriter>(m, "CsvWriter")
        .def(py::init<const std::string&>())
        .def("write", &CsvWriter::write);

  // python binding for system class
  py::class_<System>(m, "System");
  
  // python binding for system evolution class
  py::class_<SystemEvolution>(m, "SystemEvolution")
        .def("evolve", &SystemEvolution::evolve)
        .def("addCompute", &SystemEvolution::addCompute)
        .def("getSystem", &SystemEvolution::getSystem)
        .def("setNSteps", &SystemEvolution::setNSteps)
        .def("setDumpFreq", &SystemEvolution::setDumpFreq);

}
