import os
import pandas as pd
import numpy as np

def readPositions(planet_name, directory):
    """
    Reads the trajectory of a planet and make a numpy array of it.
    Returns a m x n matrix with m = 365 and n = 3, the columns being three components of the planet position. 
    """
    positions = np.empty((0, 3))
    csv_files = [file for file in os.listdir(directory) if file.endswith('.csv')]
    csv_files.sort()

    for csv_file in csv_files:
        # Read csv file
        file_path = os.path.join(directory, csv_file)
        df = pd.read_csv(file_path, sep=" ", on_bad_lines='skip', header=None)
        
        # The given trajectory has radius information but the generated files do not, need to treat differently
        if df.shape[1] == 12:
            df.columns = [' ', 'X', 'Y', 'Z', 'VX', 'VY', 'VZ', 'FX', 'FY', 'FZ', 'mass', 'name']
        else:
            df.columns = [' ', 'X', 'Y', 'Z', 'VX', 'VY', 'VZ', 'FX', 'FY', 'FZ', 'mass', 'radius', 'name']

        position = df[df.name == planet_name].loc[:, ['X', 'Y', 'Z']].to_numpy()
        positions = np.vstack((positions, position))


    return positions

def computeError(positions, positions_ref):
    """
    Compute error between two trajectories.
    """
    return np.sum(np.square(positions - positions_ref))

def generateInput(scale, planet_name, input_filename, output_filename):
    """
    Generates an input file from a given input file but scaled velocity of a given planet.
    """

    # Read csv file
    df = pd.read_csv(input_filename, sep=" ", on_bad_lines='skip', skiprows=1, header=None)
    df.columns = ['#X', 'Y', 'Z', 'VX', 'VY', 'VZ', 'FX', 'FY', 'FZ', 'mass', 'name', ' ']

    # Scale the velocity
    v = df[df.name == planet_name].loc[:, ['VX', 'VY', 'VZ']].to_numpy()
    v *= scale

    # Write back to dataframe
    row_index = df.index[df.name == planet_name].tolist()[0]

    df.loc[row_index, 'VX'] = v[0][0]
    df.loc[row_index, 'VY'] = v[0][1]
    df.loc[row_index, 'VZ'] = v[0][2]
    
    df.to_csv(output_filename, sep=' ', index=False)

def launchParticles(input_filename, timestep, nb_steps, freq):
    """
    Lauches simulation.
    """

    input = os.path.abspath(input_filename)
    os.chdir('../build/src')
    os.system(f'python main.py {nb_steps} {timestep} {input} planet {freq}')
    os.chdir('../../src')

def runAndComputeError(scale, planet_name, input_filename, input_directory, scaled_input_filename, timestep, nb_steps, freq):
    """
    Calculates error using a given scaling factor.
    """
    
    # if not os.path.exists(scaled_input_directory):
    #     os.makedirs(scaled_input_directory)

    # Generate scaled input
    generateInput(scale, planet_name, input_filename, scaled_input_filename)

    # Use scaled input to launch simulation
    launchParticles(scaled_input_filename, timestep, nb_steps, freq)
    

    # Read old trajectory
    position_old = readPositions(planet_name, input_directory)

    # Read new trajectory
    position_new = readPositions(planet_name, '../build/src/dumps')

    # Compute error
    error = computeError(position_old, position_new)

    return error



